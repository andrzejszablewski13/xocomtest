﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonAI : MonoBehaviour
{
    private Data _dataAcc;
    private float _distance, _maxDistance = 15f,_chanceToHit, _difrenceOfHigh;
    private UnityEngine.Ray _shootCaster;
    private AudioSource _audioOfShot;
    // Start is called before the first frame update
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        _audioOfShot = gameObject.GetComponent<AudioSource>();
    }
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    public void EnemyCannonAI()
    {
        for (int i = 0; i < _dataAcc.EniemiesData.GroupOfData.Count; i++)
        {
            if (_dataAcc.EniemiesData.GroupOfData[i].IsDeath == false)
            {
                PrepShot(_dataAcc.Enemy[i], i);
            
            }

        }
    }

    // Update is called once per frame
    private void PrepShot(GameObject _presCannon,int _enemyId)
    {
        for(int i=0; i<_dataAcc.PlayerDataStack.Length;i++)
        {
            if(_dataAcc.PlayerDataStack[i].IsDeath==false)
            {
                _distance= _dataAcc.DifBet2Vec2(new Vector2(_presCannon.transform.position.x, _presCannon.transform.position.z), new Vector2(_dataAcc.PlayerDataStack[i].Position.x, _dataAcc.PlayerDataStack[i].Position.z));
                _shootCaster.origin = _presCannon.transform.position;
                _shootCaster.direction = _dataAcc.PlayerDataStack[i].Position;
                if (Physics.Raycast(_shootCaster))
                {
              
                    continue;
                }else
                if (_distance<=_maxDistance)
                {
                        _difrenceOfHigh = (_presCannon.transform.position.y - _dataAcc.PlayerDataStack[i].Position.y);

                    _chanceToHit = _dataAcc.HitingProbability((int)_dataAcc.EniemiesData.GroupOfData[_enemyId].WeaponModifigher, _distance, _difrenceOfHigh);
                   
                    if (_chanceToHit>=50)
                    {
                        //startcorutin
                        StartCoroutine(ShotByCanon(_enemyId, i));
                        
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
    }

    IEnumerator ShotByCanon(int enemyID,int playerID)
    {
        _dataAcc.Player[playerID].GetComponent<PlayerControlScript>().PlayerTakeDmg(_dataAcc.EniemiesData.GroupOfData[enemyID].EnemyDmg);
        if (_audioOfShot.isPlaying == false) _audioOfShot.Play();
         yield return new WaitForSeconds(2);
    }

}
