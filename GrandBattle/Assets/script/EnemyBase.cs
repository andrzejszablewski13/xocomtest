﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    public Data _dataAcc;
    private int _posOnList;
    public Data.DataofUnit _tempSave;
    public int PosOnList { get => _posOnList;}
    private int _key;
    private EnemyData _placeToSaveDate;

    // Start is called before the first frame update
    public void EnemyBaseCreator(int temp_key)
    {
       
        _placeToSaveDate = _dataAcc.EniemiesData;
        _key = temp_key;
        _tempSave.IsDeath = false;
        _tempSave.EnemyClass = "Canon";
        _tempSave.HP = 10;
        _tempSave.WeaponModifigher = 70;
        _tempSave.EnemyDmg = 4;
        _placeToSaveDate.GroupOfData.Add(_tempSave);
        _posOnList = _placeToSaveDate.GroupOfData.Count - 1;
        
        
    }
    public void TakeDmg(int _dmg)
    {
        if (_tempSave.HP <= _dmg)
        {
            Data.SquareInGame _temp;
            _tempSave.HP -= _dmg;
           
            this.transform.position = new Vector3(0, -10, 0);
            _tempSave.IsDeath = true;
            _temp = _dataAcc.MapStack[_key];
            _temp.TypeOfObject = Data.BlockType.Empty;
            _dataAcc.MapStack[_key] = _temp;
            
            ChangeOnList();
            _dataAcc.NumberOfEnemies--;
            if (_dataAcc.NumberOfEnemies == 0)
            {
                _dataAcc.EndGame();
            }


        }
        else
        {
            _tempSave.HP -= _dmg;
            ChangeOnList();

        }
    }
    ~EnemyBase()
    {
        
        _tempSave.IsDeath = true;
        ChangeOnList();
    }
    private void ChangeOnList()
    {
        _placeToSaveDate.GroupOfData.RemoveAt(_posOnList);
        _placeToSaveDate.GroupOfData.Insert(_posOnList, _tempSave);
    }
}
