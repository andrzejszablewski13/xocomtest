﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IBaseForCharacterClassSystem
    {
    void ActiveDecisionSet();
    }
public class BaseForClass : MonoBehaviour
{
    private Data _dataAcc;
    protected static GameObject _canvasSpace,_decisionSpace;
    protected static Dictionary<string, GameObject> _posibleDecision;
    // Start is called before the first frame update
    protected void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
        _canvasSpace = _dataAcc.canvasObject;
    }
    protected void ForAwakeForAll()
    {
        Data.AccessToData += GetAccessToData;
    }
    private void Awake()
    {
    ForAwakeForAll();
    }
    public static void TakeLokationOfDecision()
    {
        int childCount = _canvasSpace.transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            if(_canvasSpace.transform.GetChild(i).name== "DecisionSystem")
            {
                _decisionSpace = _canvasSpace.transform.GetChild(i).gameObject;
            }
        }
        _posibleDecision = new Dictionary<string, GameObject>();
        for (int i = 0; i < _decisionSpace.transform.childCount; i++)
        {
            _posibleDecision.Add(_decisionSpace.transform.GetChild(i).name, _decisionSpace.transform.GetChild(i).gameObject);
        }

    }
    public static void DeactiveDeccisionSet()
    {
        for (int i = _decisionSpace.transform.childCount-1; i >=0; i--)
        {
            _decisionSpace.transform.GetChild(i).gameObject.SetActive(false);
        }
        
    }
    
}
