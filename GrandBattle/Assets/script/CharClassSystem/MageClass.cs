﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageClass : BaseForClass,IBaseForCharacterClassSystem
{
    public string NameOfClass = "Mage";
    private void Awake()
    {
        base.ForAwakeForAll();
    }
    public  void ActiveDecisionSet()
    {
       
        _posibleDecision["Move"].SetActive(true);
        _posibleDecision["Attack"].SetActive(true);
        _posibleDecision["Spell"].SetActive(true);
    }
}
