﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "EnemyData")]
public class EnemyData : ScriptableObject
{
    // Start is called before the first frame update
    
    
    public List<Data.DataofUnit> GroupOfData = new List<Data.DataofUnit>();
}
