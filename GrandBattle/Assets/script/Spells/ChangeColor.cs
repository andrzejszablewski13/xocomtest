﻿using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    private Color _colorSave;
    private Renderer _rend;
    private bool _ifBlack;
    private void Awake()
    {
        if(this.gameObject.TryGetComponent<Renderer>(out Renderer _temp))
        { 
            _rend = _temp;
        }
        else
        {
            _rend = this.gameObject.GetComponentInChildren<Renderer>();
        }
        if (_rend.material.color != Color.black)
        {
            _colorSave = _rend.material.color;
            _rend.material.color = Color.black;
            _ifBlack = false;
        }
        else
        {
            _ifBlack = true;
        }
    }
    public void CustomDestroy()
    {
        if (_ifBlack == false)
        { 
            _rend.material.color = _colorSave; 
        }
        Destroy(this);
    }
}
