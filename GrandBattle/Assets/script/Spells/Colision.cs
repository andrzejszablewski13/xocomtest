﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colision : MonoBehaviour
{
    [HideInInspector]public Data _dataAcc;
    private Data.SquareInGame _temporalData;
    private int _key;
    // Start is called before the first frame update
    private readonly List<Collider> TriggerList = new List<Collider>();
    private void Awake()
    {
        Ray.UseOrFire += Fire;
        
    }
    private void DestroyPosinDictionary(Vector3 _forKeys)
    {
        
        if (_forKeys.y % 1 == 0)
        {
            _key = _dataAcc.Ultramuch + (int)_forKeys.x * _dataAcc.VeryMuch + (int)_forKeys.y * _dataAcc.Much + (int)_forKeys.z;
            try
            {
                
                for (int i = 0; i < _dataAcc.MapStack[_key].NeighborsProbablyFree.Count; i++)
                {
                    _temporalData = _dataAcc.MapStack[_dataAcc.MapStack[_key].NeighborsProbablyFree[i]];
                    if (_temporalData.NeighborsProbablyFree.Contains(_key))
                    {
                        _temporalData.NeighborsProbablyFree.Remove(_key);
                        //Debug.Log(_key);
                    }
                    _dataAcc.MapStack[_dataAcc.MapStack[_key].NeighborsProbablyFree[i]] = _temporalData;
                    //Debug.Log(_dataAcc.MapStack[_dataAcc.MapStack[_key].NeighborsProbablyFree[i]].NeighborsProbablyFree.Count);
                }
            }
            catch (System.Exception)
            {

                Debug.Log(_key);
            }
            
        }
    }
    private void ChangeToFree(Vector3 _forKeys)
    {
        _key = _dataAcc.Ultramuch + (int)_forKeys.x * _dataAcc.VeryMuch + (int)_forKeys.y * _dataAcc.Much + (int)_forKeys.z;
        try
        {
            _temporalData = _dataAcc.MapStack[_key];
            _temporalData.TypeOfObject = Data.BlockType.Empty;
            _dataAcc.MapStack[_key] = _temporalData;
        }
        catch (System.Exception)
        {
            Debug.Log(_key);

        }
    }
        
    private void Fire()
    {
        for (int i = 0; i < TriggerList.Count; i++)
        {
            if(TriggerList[i].tag!="Player"&& TriggerList[i].tag !="Enemy"&& TriggerList.Contains(TriggerList[i].GetComponentInParent<Collider>()))
            {
                Destroy(TriggerList[i].gameObject);
                if(TriggerList[i].gameObject.transform.position.y>0 && TriggerList[i].gameObject.tag!="Untagged")
                {
                    DestroyPosinDictionary(TriggerList[i].gameObject.transform.position);
                    //Debug.Log(TriggerList[i].gameObject.name);
                }else if(TriggerList[i].gameObject.transform.position.y == 0)
                {
                    ChangeToFree(TriggerList[i].gameObject.transform.position);
                }
            }
            else if(TriggerList[i].CompareTag("Player"))
            {
                TriggerList[i].GetComponent<PlayerControlScript>().PlayerTakeDmg(15);
            }
            else
            {
                TriggerList[i].GetComponent<EnemyBase>().TakeDmg(15);
            }
        }
        TriggerList.Clear();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Plane")&&!(other.gameObject.TryGetComponent<ChangeColor>(out _)))
        {
            other.gameObject.AddComponent<ChangeColor>();
            if (!TriggerList.Contains(other))
            {
                //add the object to the list
                TriggerList.Add(other);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Plane")&& other.gameObject.TryGetComponent<ChangeColor>(out ChangeColor _temp))
        {
            if (TriggerList.Contains(other))
            {
                _temp.CustomDestroy();
                //remove it from the list
                TriggerList.Remove(other);
            }
            
        }
    }

  
}
