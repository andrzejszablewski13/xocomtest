﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Graf : MonoBehaviour
{

    private Data _dataAcc;

    
    private Data.BlockType[,,] _stack2;
    private int _activePlayer = 0;
    private readonly List<Vector3Int> _enemySpawn= new List<Vector3Int>();
    private GameObject[] _primaryStack;
    private int _grafTemp11, _grafTemp2, _grafTemp3;
    private int _tempKey,_tempKey2;
    private Data.SquareInGame _tempClass;
    private List<Vector3> _listofLaber;
    private Vector3 _tempVec;
    private int _mapSize1, _mapSize2, _mapSize3;
    private GameObject _temp;//for enemy spawn
    private int _tempForEnemy;
    private Vector3Int _posIntForEnemy;


    // Start is called before the first frame update
    public void Awake()
    {
        Data.StrategicMapStart += WorldLoad;
        Data.AccessToData += GetAccessToData;
    }
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    public void WorldLoad()
    {
        //access to data   
        
        
        _dataAcc.Rekustarttest = true;
        _listofLaber = new List<Vector3>();
    //set first player as active
        
        
       
        
        //get all prepared elemont on scene
        _primaryStack = gameObject.GetAllChilds().ToArray();
        
      //set map max size
        if (_primaryStack[0].name == "Floor")//first oject is always plane to st max size in x and z direction
        {
            _mapSize1 = Mathf.RoundToInt(_primaryStack[0].GetComponent<Transform>().localScale.x) * 10;
            _mapSize2 = Mathf.RoundToInt(_primaryStack[0].GetComponent<Transform>().localScale.z) * 10 ;
        }
        _mapSize3 = 3;

        //create array to collect all elemental information about map
        _stack2 = new Data.BlockType[_mapSize1, _mapSize3, _mapSize2];
       
        for (int i=0; i<_primaryStack.Length;i++)
        {//set starting map information
            _grafTemp11 = Mathf.RoundToInt(_primaryStack[i].GetComponent<Transform>().position.x / _dataAcc.SizeX);
            _grafTemp2 = Mathf.RoundToInt(_primaryStack[i].GetComponent<Transform>().position.z/ _dataAcc.SizeZ);
            _grafTemp3 = Mathf.RoundToInt(_primaryStack[i].GetComponent<Transform>().position.y / _dataAcc.SizeY);
            if (_primaryStack[i].GetComponent<MeshFilter>().mesh.name == "Plane Instance")
            {//set for plane
                for (int j = 0;  j < _mapSize1;j++)
        {
                    for (int k = 0; k < _mapSize2; k++)
                    {
                        _stack2[j,0,k]= Data.BlockType.Empty;
                    }
                }
            }else
            {
                if (_primaryStack[i].CompareTag(Data.Tags.Empty.ToString()))
                {
                    //set this space as empy(possibl to move)
                    _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Empty;
                
                }
                else
                    if (_primaryStack[i].CompareTag(Data.Tags.Block.ToString()))
                {//set as blocked by something
                    _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Block;
                }
                else
                if (_primaryStack[i].CompareTag(Data.Tags.PlayerUnder.ToString()))
                {
                    AddPlayebleCharacter(_primaryStack[i].GetComponent<Transform>().position);
                }
                else if (_primaryStack[i].CompareTag(Data.Tags.Laber.ToString()))
                {//set this space as ladder
                    _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Laber;
                    _listofLaber.Add(new Vector3(_grafTemp11, _grafTemp3, _grafTemp2));
                    
                }
                else if (_primaryStack[i].CompareTag(Data.Tags.EnemySpawn.ToString()))//space of enemy spawning of group of -/-
                {
                    _enemySpawn.Add(new Vector3Int(_grafTemp11, _grafTemp3, _grafTemp2));
                    
                }
                else
                {//not existing space (empty and can go to it)

                    _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Void;
                }
            }
            
            
        }
        _dataAcc.Player = _dataAcc.PlayerStack.GetAllChilds().ToArray();
        _dataAcc.RayHitName = _dataAcc.Player[0].name;
        _activePlayer = 0;
        CreateDictionary();
        EnemySpawn();
        
        _dataAcc.Enemy = _dataAcc.EnemyParent.GetAllChilds().ToArray();//get list of all enemies
        _dataAcc.NumberOfEnemies = _dataAcc.Enemy.Length;
        
        _dataAcc.GameTurn = Data.Turn.PlayerTurn;//begin player turn
        _dataAcc.MovingScript.GetComponent<MovingUI>().BeginingOfScript();
        _dataAcc.RekuMoveAcc.MovementOnDictionary();
    }
    
    public void CreateDictionary()//as name say
    {
        _dataAcc.MapStack = new Dictionary<int, Data.SquareInGame>();//create structs in dictionary
        _tempClass = new Data.SquareInGame();
        for (int i = 0; i < _mapSize3; i++)
        {
            for (int j = 0; j < _mapSize1; j++)
            {
                for (int k = 0; k < _mapSize2; k++)
                {
                    if (_stack2[j, i, k] == Data.BlockType.Empty || _stack2[j, i, k] == Data.BlockType.Player || _stack2[j, i, k] == Data.BlockType.Block)
                    {
                        _tempKey =_dataAcc.Ultramuch+j * _dataAcc.VeryMuch + i * _dataAcc.Much + k;
                        _tempClass.Key = _tempKey;
                        _tempClass.NeighborsProbablyFree = new List<int>();
                        _tempClass.Steps = 0;
                        _tempClass.MoveSqarePossed = false;
                        _tempClass.TypeOfObject = _stack2[j, i, k];
                        _dataAcc.MapStack.Add(_tempKey, _tempClass);
                        
                    }
                }
            }
        }
      
        for (int i = 0; i < _mapSize3; i++)//create ref to structs neighbours
        {
            for (int j = 0; j < _mapSize1; j++)
            {
                for (int k = 0; k < _mapSize2; k++)
                {
                    _tempKey = _dataAcc.Ultramuch + j * _dataAcc.VeryMuch + i * _dataAcc.Much + k;
                    if (_dataAcc.MapStack.ContainsKey(_tempKey))
                    {
                        _tempKey2 = _dataAcc.Ultramuch + (j+1) * _dataAcc.VeryMuch + i * _dataAcc.Much + k;
                        AddNeighborsToDic();
                        _tempKey2 = _dataAcc.Ultramuch + (j -1) * _dataAcc.VeryMuch + i * _dataAcc.Much + k;
                        AddNeighborsToDic();
                        _tempKey2 = _dataAcc.Ultramuch + j * _dataAcc.VeryMuch + i * _dataAcc.Much + k+1;
                        AddNeighborsToDic();
                        _tempKey2 = _dataAcc.Ultramuch + j * _dataAcc.VeryMuch + i * _dataAcc.Much + k-1;
                        AddNeighborsToDic();
                    }

                }
            }
        }
        for (int i = 0; i < _listofLaber.Count; i++)//create ref to structs neighbours by laber
        {
            _tempVec = _listofLaber[i];
            _tempKey =(int)(_dataAcc.Ultramuch + _tempVec.x  * _dataAcc.VeryMuch + (_tempVec.y-1)* _dataAcc.Much + _tempVec.z);
            if(_dataAcc.MapStack.ContainsKey(_tempKey))
            {
                _tempKey2= (int)(_dataAcc.Ultramuch + (_tempVec.x+1) * _dataAcc.VeryMuch + (_tempVec.y + 1) * _dataAcc.Much + _tempVec.z);
                if(_dataAcc.MapStack.ContainsKey(_tempKey2))
                    {
                    AddNeighbourFromLaber();
                    }
                _tempKey2 = (int)(_dataAcc.Ultramuch + (_tempVec.x -1) * _dataAcc.VeryMuch + (_tempVec.y + 1) * _dataAcc.Much + _tempVec.z);
                if (_dataAcc.MapStack.ContainsKey(_tempKey2))
                {
                    AddNeighbourFromLaber();
                }
                _tempKey2 = (int)(_dataAcc.Ultramuch + _tempVec.x * _dataAcc.VeryMuch + (_tempVec.y + 1) * _dataAcc.Much + _tempVec.z+1);
                if (_dataAcc.MapStack.ContainsKey(_tempKey2))
                {
                    AddNeighbourFromLaber();
                }
                _tempKey2 = (int)(_dataAcc.Ultramuch + _tempVec.x * _dataAcc.VeryMuch + (_tempVec.y + 1) * _dataAcc.Much + _tempVec.z - 1);
                if (_dataAcc.MapStack.ContainsKey(_tempKey2))
                {
                    AddNeighbourFromLaber();
                }

            }
        }
    }
    void AddNeighbourFromLaber()//as name say
    {
        _tempClass = _dataAcc.MapStack[_tempKey];
        _tempClass.NeighborsProbablyFree.Add( _tempKey2);
        _dataAcc.MapStack[_tempKey] = _tempClass;
        _tempClass = _dataAcc.MapStack[_tempKey2];
        _tempClass.NeighborsProbablyFree.Add(_tempKey);
        _dataAcc.MapStack[_tempKey2] = _tempClass;
    }
    public void AddNeighborsToDic()//as name say
    {
        if (_dataAcc.MapStack.ContainsKey(_tempKey2))
        {
           
            
                _dataAcc.MapStack[_tempKey].NeighborsProbablyFree.Add(_dataAcc.MapStack[_tempKey2].Key);
            
        }
    }
    public void AddPlayebleCharacter(Vector3 _position)
    {
        if (_activePlayer < _dataAcc.PlayerDataStack.Length)//if there are free (yet not spawned) player
        {//spawn player ans set this space as player
            /*_dataAcc.PlayerDataStack[_activePlayer].Position = _position;
            _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Player;
            _dataAcc.Player[_activePlayer].transform.position = _dataAcc.PlayerDataStack[_activePlayer].Position;*/
            _dataAcc.PlayerDataStack[_activePlayer].Position = _position;
            _dataAcc.PlayerDataStack[_activePlayer].IsDeath = false;
            _dataAcc.PlayerDataStack[_activePlayer].HP = _dataAcc.PlayerDataStack[_activePlayer].MaxHP;
            _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Player;
            _temp = Instantiate((GameObject)_dataAcc.DicOfPlayers[_dataAcc.PlayerDataStack[_activePlayer].NameOfPreFab], _position, Quaternion.identity, _dataAcc.PlayerStack.transform);
            _temp.GetComponent<PlayerControlScript>().NumberOfThisPlayer = _activePlayer;
            _temp.GetComponent<PlayerControlScript>().DataOfCharacter = _dataAcc.PlayerDataStack[_activePlayer];
            _temp.transform.position = _position;
            _temp.name = "Player" + _activePlayer.ToString();
            _activePlayer++;
        }//if there are not free player set space as free
        else { _stack2[_grafTemp11, _grafTemp3, _grafTemp2] = Data.BlockType.Empty; }
    }


    public void EnemySpawn()//as name say
    {
        for (int i = 0; i < _enemySpawn.Count; i++)
        {

           
        _tempForEnemy = Random.Range(0, _dataAcc.EnemiesPrefab.Length);//set random type of enemy
        _posIntForEnemy = _enemySpawn[i];//set primary spawn position
        PlaceEnemyInDictionary();
  
        _temp =Instantiate((GameObject)_dataAcc.EnemiesPrefab[_tempForEnemy],_posIntForEnemy, Quaternion.identity, _dataAcc.EnemyParent.transform);//EnemySpawn first enemy
        _temp.AddComponent<EnemyBase>()._dataAcc=_dataAcc;
        
        _temp.GetComponent<EnemyBase>().EnemyBaseCreator(_tempKey);
        
        
        
        if (_stack2[_posIntForEnemy.x+1, _posIntForEnemy.y, _posIntForEnemy.z]==Data.BlockType.Empty)//generate more enemies in around of deleted spawn point (if there is free space)
        {
            _posIntForEnemy.Set(_posIntForEnemy.x + 1, _posIntForEnemy.y, _posIntForEnemy.z);
            PlaceEnemyInDictionary();
            _tempForEnemy = Random.Range(0, _dataAcc.EnemiesPrefab.Length);
            _temp=Instantiate((GameObject)_dataAcc.EnemiesPrefab[_tempForEnemy], _posIntForEnemy, Quaternion.identity, _dataAcc.EnemyParent.transform);
            _temp.AddComponent<EnemyBase>()._dataAcc = _dataAcc;
            _temp.GetComponent<EnemyBase>().EnemyBaseCreator(_tempKey);
        }
        }
        _enemySpawn.Clear();
    }
    private void PlaceEnemyInDictionary()
    {
        _tempKey = _dataAcc.Ultramuch + _posIntForEnemy.x * _dataAcc.VeryMuch + _posIntForEnemy.y * _dataAcc.Much + _posIntForEnemy.z;
        _tempClass = _dataAcc.MapStack[_tempKey];
        _tempClass.TypeOfObject = Data.BlockType.Block;
        _dataAcc.MapStack[_tempKey] = _tempClass;

    }
    // Update is called once per frame
   
    
    
}

