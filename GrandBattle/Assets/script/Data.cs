﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Data : MonoBehaviour
{

    // access to all other script in scene
    [SerializeField] public GameObject RekuMoveAccess, RayAccess, PlayerStack, AttackSystem, CameraFollow, canvasObject, animatorControler, MovingScript,CannonAI;

    private RekuMove _RekuMoveAcc;


    public RekuMove RekuMoveAcc { get => _RekuMoveAcc; set => _RekuMoveAcc = value; }


    
    public Stack<Vector3> MovementStackCollection;//for animation-creatin a road

    public enum BlockType : byte//type of square
    {
        Void, Empty, Player, Block, Laber
    };


    [SerializeField] public GameObject RekuRangeObject;//place where script movement are

    private int _activePlayer = 0, _activeEWnemy = 0;


    private bool _rekustarttest;
    public int ActivePlayer { get => _activePlayer; }//active player from array

    public int ActiveEWnemy { get => _activeEWnemy; set => _activeEWnemy = value; }//active enemy from array


    public bool Rekustarttest { get => _rekustarttest; set => _rekustarttest = value; }//security for start recurtion of movement tracking
    public Dictionary<int, SquareInGame> MapStack;//dictionary for movement
    public struct SquareInGame//struct for dictionary for movement
    {
        public BlockType TypeOfObject;//as name say
        public int Key;//key of this truct in dictionary
        public List<int> NeighborsProbablyFree;//list of ref to struct around this
        public byte Steps;//both to movement
        public int Previus;
        public bool MoveSqarePossed;


    }


    private Vector3 _moousePosition, _mousePositionWOClick;//security for movement tracking and moveing vector
    private string _RayHitName;//find active player object(if change active player)
    private bool _RayHitNameTest = false;//security for find active player object
    private int _Sixex = 1, _sizey = 1, _sizeZ = 1;//size of one square (now 1 on 1 on1)


    public Vector3 MoousePosition { get => _moousePosition; set => _moousePosition = value; }
    public Vector3 MousePositionWClick { get => _mousePositionWOClick; set => _mousePositionWOClick = value; }
    public string RayHitName { get => _RayHitName; set => _RayHitName = value; }
    public bool RayHitNameTest { get => _RayHitNameTest; set => _RayHitNameTest = value; }
    public int SizeX { get => _Sixex; set => _Sixex = value; }
    public int SizeY { get => _sizey; set => _sizey = value; }
    public int SizeZ { get => _sizeZ; set => _sizeZ = value; }

    private float _distancePlayerEnemy;
    [SerializeField] private float _modofDistance = 100, _modofWepon = 1, _modofHeigh = 1;//data to hit probability calculation
    public float DistancePlayerEnemy { get => _distancePlayerEnemy; set => _distancePlayerEnemy = value; }//data to hit probability calculation



    public enum Turn : byte//stage of game
    {
        Start, PlayerTurn, EnemyTurn, End, Prep
    }
    private Turn _GameTurn;

    public Turn GameTurn { get => _GameTurn; set => _GameTurn = value; }
    [SerializeField] public GameObject EnemyParent;//place where enemies instanties are stacked
    [HideInInspector] public GameObject[] Enemy, Player;//arrays of reference to enemy/players objects
    private RectTransform CanvasRect { get; set; }//size of canvas
    private float _chanceToHit;
    public float ChanceToHit { get => _chanceToHit; set => _chanceToHit = value; }//probability of hit
   
    private int _numberOfEnemies, _numberOfPlayers;

    public int NumberOfEnemies { get => _numberOfEnemies; set => _numberOfEnemies = value; }
    public int NumberOfPlayers { get => _numberOfPlayers; set => _numberOfPlayers = value; }

    public PlayerData[] PlayerDataStack;
    public SaveData SaveLokation;
    public EnemyData EniemiesData;//to save/locate data of players+enemies

    public struct DataofUnit//to save/locate data of players+enemies-primaly contairnar
    {
        public int IDofEnemy;
        public string EnemyClass;
        public int HP;
        public bool IsDeath;
        public float WeaponModifigher;
        public int EnemyDmg;

    }

    private int _ultramuch, _veryMuch, _much;

    public int Ultramuch { get => _ultramuch; }//fo key creation
    public int VeryMuch { get => _veryMuch; }
    public int Much { get => _much; }
    public static Action<Data> AccessToData;
    public static Action StrategicMapStart;
    public enum Tags
    {
        Empty,Block, PlayerUnder, Laber, Player, EnemySpawn
    }
    void Start()
    {//active game
        _ultramuch = (int)(9 * Mathf.Pow(10, 8));
        _veryMuch = (int)(Mathf.Pow(10, 5));
        _much = (int)Mathf.Pow(10, 3);
        _GameTurn = Turn.Prep;
        //complete preparing data
        CanvasRect = canvasObject.GetComponent<RectTransform>();
        _textOfEndOfGame = canvasObject.GetComponentInChildren<EndGame>();
        _textOfEndOfGame.gameObject.SetActive(false);
        NumberOfPlayers = Player.Length;
        /*for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.tags.Length; i++)
        {
            Debug.Log(UnityEditorInternal.InternalEditorUtility.tags[i]);
        }*/
        


        _RekuMoveAcc = RekuMoveAccess.GetComponent<RekuMove>();

        LoadPrefabs();
        ActionReset();//for sure reset player data
        AccessToData(this);
        StrategicMapStart();
        AccessToData(this);
        BaseForClass.TakeLokationOfDecision();
        ChangePosibleDecision();
    }
    public void ChangePosibleDecision()
    {

        BaseForClass.DeactiveDeccisionSet();
        Player[_activePlayer].SendMessage("ActiveDecisionSet");
        //Player[_activePlayer].GetComponent(_className).DeactiveDeccisionSet();
    }

    [HideInInspector] public UnityEngine.Object[] EnemiesPrefab, PlayersPreFab;
    [HideInInspector] public UnityEngine.Object SpellRangePreFab,RangeForSpellPreFab;
    public Dictionary<string, UnityEngine.Object> DicOfPlayers;
    private GameObject _spellsRangeObject,_rangeOfSpell;
    public GameObject SpellsRangeObject { get => _spellsRangeObject; }

    public void LoadPrefabs()
    {
        DicOfPlayers = new Dictionary<string, UnityEngine.Object>();
        //_enemiesPrefab = new GameObject[2];
        EnemiesPrefab = Resources.LoadAll("prefab/enemies");
        PlayersPreFab = Resources.LoadAll("prefab/players");
        SpellRangePreFab = Resources.Load("prefab/Spells/SpellRange");
        RangeForSpellPreFab = Resources.Load("prefab/Spells/SpellUseRange");
        for (int i = 0; i < PlayersPreFab.Length; i++)
        {
            DicOfPlayers.Add(PlayersPreFab[i].name, PlayersPreFab[i]);

        }
    }

    public void ResetEnemiesNumber()//as name say
    {

        _activeEWnemy = 0;
        while (EniemiesData.GroupOfData[ActiveEWnemy].IsDeath == true)
        {
            _activeEWnemy++;
        }
    }

    public void DestroyChildren(Transform root)//as name say
    {
        //get number of children
        int childCount = root.childCount;
       
        //destroy all
        for (int i = childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(root.GetChild(i).gameObject);
        }
    }
    public void ChangePlayerCharacter()//as name say
    {
        //if player wa clicked
        if (RayHitNameTest == true)
        {
            //for all players
            for (int i = 0; i < Player.Length; i++)
            {//check if this player wa hited witch click
                if (RayHitName != Player[i].name) { continue; } else { _activePlayer = i; break; }
            }
            StartCoroutine(RestartMove());
            //deactivated checking if player was clicked
            RayHitNameTest = false;
            ChangePosibleDecision();
            

        }
    }
    private int _forChangeNextPla=0;
    public void ChangeToNextPlayerCharacter()//as name say
    {
        _activePlayer++;
        if(CheckIfGood()==false && _forChangeNextPla<= Player.Length)
        {
            ChangeToNextPlayerCharacter();
        }
        else
        {
            _forChangeNextPla = 0;
        }

    }
    private bool CheckIfGood()
    {
        if(_activePlayer< Player.Length) { return false; }
        if (PlayerDataStack[_activePlayer].IsDeath==true) { return false; }
        return true;
    }
    public void TurnChange()//as name say
    {//if its player turn
        if (_GameTurn.Equals(Turn.PlayerTurn))
        {//change turn to enemy turn
            _GameTurn = Turn.EnemyTurn;
            //reset settings of player to default to prepare for next turn 
            Rekustarttest = false;
            //DestroyChildren(RekuRangeObject.transform);
            RekuMoveAcc.RecycleSquere();
            ActionReset();
            //activate AI
            TempAI();
        }
    }
    public void TempAI()//as name say
    {

        CannonAI.GetComponent<CanonAI>().EnemyCannonAI();
        Rekustarttest = true;
        
        _GameTurn = Turn.PlayerTurn;
        RekuMoveAcc.MovementOnDictionary();
    }

    public void ActionReset()//as name say
    {
        //_decisionSet = DecisionType.Move;
        //reset action for all players
        for (int i = 0; i < PlayerDataStack.Length; i++)
        {
            PlayerDataStack[i].Actions = 2;
            PlayerDataStack[i].LeftMoves = PlayerDataStack[i].MaxMoves;
        }

    }
    public enum DecisionType : byte//to preserve type of actual posiible decision
    {
        Move, Attack,Spell
    }
    private DecisionType _decisionSet = DecisionType.Move;
    public DecisionType DecisionSet { get => _decisionSet; set => _decisionSet = value; }
    private int _decisionSetButton;
    public int DecisionSetButton { get => _decisionSetButton; set => _decisionSetButton = value; }//do aktywacji trybu (Strzelanie,ruch)
   
    public static Action OpenShotingUI, CloseShotingUI;
    public static Action<bool> OpenCloseMovingUI;
    IEnumerator RestartMove()
    {
        //destroy move calculation for old player
        RekuMoveAcc.RecycleSquere();
        yield return new WaitForSeconds(0.002f);
        //create new move calculation
        _rekustarttest = true;
        RekuMoveAcc.MovementOnDictionary();
    }
    public void ChangeDecisionSet()//as name say
    {
        //deactivate all component that are part of any decision
        
        RekuMoveAccess.SetActive(false);
        AttackSystem.SetActive(false);
        CloseShotingUI();
        StopCoroutine(RayAccess.GetComponent<Ray>().Corutin);      
        OpenCloseMovingUI(false);
        try
        {
            _spellsRangeObject.gameObject.transform.position=new Vector3(0,100,0);
            Destroy(_spellsRangeObject.gameObject,0.1f);
            Destroy(_rangeOfSpell);
        }
        catch (Exception)
        {
        }
        // canvasObject.GetComponentInChildren<ShootingUI>().gameObject.SetActive(false);
        switch (_decisionSetButton)
        {
            case 0://if move was decided
                _decisionSet = DecisionType.Move;
                ChangeCamera();
                RekuMoveAccess.SetActive(true);              
                OpenCloseMovingUI(true);
                StartCoroutine(RayAccess.GetComponent<Ray>().Corutin);
                StartCoroutine(RestartMove());
                break;
            case 1://if normal attack was decided
                _decisionSet = DecisionType.Attack;
                ChangeCamera();
                AttackSystem.SetActive(true);
                //canvasObject.GetComponentInChildren<ShootingUI>().gameObject.SetActive(true);
                OpenShotingUI();
                if (PlayerDataStack[ActivePlayer].Actions <= 0)
                {
                    _decisionSetButton = 0;
                    ChangeDecisionSet();
                }
                break;
            case 2://if move was decided
                _decisionSet = DecisionType.Spell;
                _spellsRangeObject = Instantiate((GameObject)SpellRangePreFab, MousePositionWClick, Quaternion.identity);
                _rangeOfSpell = Instantiate((GameObject)RangeForSpellPreFab, PlayerDataStack[ActivePlayer].Position, Quaternion.identity);
                _spellsRangeObject.GetComponent<Colision>()._dataAcc = this;
                ChangeCamera();
                OpenCloseMovingUI(true);
                StartCoroutine(RayAccess.GetComponent<Ray>().Corutin);
                if (PlayerDataStack[ActivePlayer].Actions<=0 )
                {
                    _decisionSetButton = 0;
                    ChangeDecisionSet();
                }
                break;
            default:
                break;

        }
        //change camera

    }
    public void ChangeCamera()//as name say
    {
        //deactivate (for sure) all existing cameras
        StopCoroutine(CameraFollow.GetComponent<CameraMove>().Corutin);
        for (int i = 0; i < Player.Length; i++)
        {
            Player[i].GetComponent<PlayerControlScript>().ActiveOrDeactivateShootMode(false);
        }
    
        CameraFollow.SetActive(false);
        switch (_decisionSet)//active proper(one) camera
        {
            case DecisionType.Move:
                CameraFollow.SetActive(true);
                StartCoroutine(CameraFollow.GetComponent<CameraMove>().Corutin);
                break;
            case DecisionType.Attack:
                Player[_activePlayer].GetComponent<PlayerControlScript>().ActiveOrDeactivateShootMode(true);             
                break;
            case DecisionType.Spell:
                CameraFollow.SetActive(true);
                StartCoroutine(CameraFollow.GetComponent<CameraMove>().Corutin);
                break;
            default:
                break;
        }
    }
    private float _propablity;
    
    public float HitingProbability(int _shoterMod, float _distance, float _heighdifrence)//as name say
    {//calculate propability of hitung by normal shoting

        _propablity = ((_modofWepon * _shoterMod) + (_heighdifrence * _modofHeigh)) / (_distance / _modofDistance);
        return _propablity;
    }
    private float offsetPosY;
    private Vector3 offsetPos;
    private Vector2 screenPoint;

    public Vector2 AttachUIToGameObject(Vector3 _targetGameObject, float high)//as name say
    {
        // Offset position above object bbox (in world space)
        offsetPosY = _targetGameObject.y + high;

        // Final position of marker above GO in world space
        offsetPos = new Vector3(_targetGameObject.x, offsetPosY, _targetGameObject.z);

        // Calculate *screen* position (note, not a canvas/recttransform position)
        screenPoint = Camera.allCameras[0].WorldToScreenPoint(offsetPos);

        // Convert screen position to Canvas / RectTransform space <- leave camera null if Screen Space Overlay
        RectTransformUtility.ScreenPointToLocalPointInRectangle(CanvasRect, screenPoint, null, out Vector2 canvasPos);

        // Set

        return canvasPos;
    }
    private EndGame _textOfEndOfGame;
    public void EndGame()
    {
        _decisionSet = DecisionType.Move;
        _GameTurn = Turn.End;
        StopCoroutine(RayAccess.GetComponent<Ray>().Corutin);
        _textOfEndOfGame.gameObject.SetActive(true); ;
        if(_numberOfEnemies==0)
        {
            _textOfEndOfGame.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "YOU WIN !!!";
        }else
        if (_numberOfPlayers == 0)
        {
            _textOfEndOfGame.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "YOU LOST !!!";
        }
    }
    public float DifBet2Vec2(Vector2 i, Vector2 j)//distance beewen to vectors in 2d
    {
        float t = Mathf.Sqrt(Mathf.Abs(Mathf.Pow(i.x, 2) - Mathf.Pow(j.x, 2)) + Mathf.Abs(Mathf.Pow(i.y, 2) - Mathf.Pow(j.y, 2)));
        return t;
    }
}
public static class ClassExtension
{
    public static List<GameObject> GetAllChilds(this GameObject Go)//as name say
    {//create list
        List<GameObject> list = new List<GameObject>();
        for (int i = 0; i < Go.transform.childCount; i++)//for all chidren of gameobject
        {//add reference of every child to list
            list.Add(Go.transform.GetChild(i).gameObject);
        }//return list of gameobjects(childs)
        return list;
    }
}