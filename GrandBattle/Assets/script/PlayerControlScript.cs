﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlScript : MonoBehaviour
{
    // Start is called before the first frame update
    private Camera _cameraPlayer;
    private AudioListener _ListenerOnCameraPlayer;
    private AudioSource _sourceOfAudioOnPlayer;
    private Data _dataAcc;
    [HideInInspector] public int NumberOfThisPlayer;
    private Data.SquareInGame _temp;
    private int _key;
    [HideInInspector] public string ClassOfPlayer;
    [HideInInspector] public PlayerData DataOfCharacter;
    
    private  void Awake()
    {
        BaseForAwake();
        ClassOfPlayer = "Base";
    }
    private void  GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    private void BaseForAwake()
    {
        Data.AccessToData += GetAccessToData;
        _cameraPlayer = GetComponentInChildren<Camera>();
        _ListenerOnCameraPlayer = GetComponentInChildren<AudioListener>();
        _sourceOfAudioOnPlayer = GetComponentInChildren<AudioSource>();
    }
    public void ActiveOrDeactivateShootMode(bool _setActice)
    {
        _cameraPlayer.enabled = _setActice;
        _ListenerOnCameraPlayer.enabled = _setActice;
        _sourceOfAudioOnPlayer.enabled = _setActice;
    }
    public void PlayerTakeDmg(int _dmg)
    {
        if (DataOfCharacter.HP <= _dmg)
        {

            DataOfCharacter.HP -= _dmg;

            
            DataOfCharacter.IsDeath = true;
            _key = _dataAcc.Ultramuch + (int)DataOfCharacter.Position.x * _dataAcc.VeryMuch + (int)DataOfCharacter.Position.y * _dataAcc.Much + (int)DataOfCharacter.Position.z;
            
            _temp = _dataAcc.MapStack[_key];
            _temp.TypeOfObject = Data.BlockType.Empty;
            _dataAcc.MapStack[_key] = _temp;
            this.transform.position = new Vector3(0, -10, 0);

            _dataAcc.NumberOfPlayers--;
            if (_dataAcc.NumberOfPlayers == 0)
            {
                _dataAcc.EndGame();
            }


        }
        else
        {
            DataOfCharacter.HP -= _dmg;

        }
    }
}
