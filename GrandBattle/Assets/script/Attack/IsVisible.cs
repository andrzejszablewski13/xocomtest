﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsVisible : MonoBehaviour
{
    // Start is called before the first frame update
    //private Renderer m_Renderer;
    private Vector3 _startPos, _endPos,_bonusPoint;
    [SerializeField] private int _visionRange=25;   
    private RaycastHit _hit2;
    private bool __visibleBy;
    // Use this for initialization
    void Start()
    {   
        _startPos = this.gameObject.transform.position;
        _startPos.y += 0.5f;
        _endPos = Camera.allCameras[0].transform.position;
        //m_Renderer = GetComponent<Renderer>();       
    }
public bool IsInEyeRange(Vector3 _playerPos)//track if enemy can be visible by camere (attack mode+allowing camera to rotate)
    {
        _endPos = _playerPos;
        _endPos.Set(_endPos.x, _endPos.y + 1f, _endPos.z);
        _bonusPoint = _endPos - _startPos;
       
        if (Vector3.Distance(_endPos, _startPos) <= _visionRange)
        {
            if (Physics.Raycast(_startPos, _bonusPoint, out _hit2, _visionRange))
            {
               // Debug.Log("testhit "+ _hit2.collider.name);
                if(_hit2.collider.CompareTag("Player") || _hit2.collider.CompareTag("Enemy"))
                {
                    GameObject _temp;
                    _temp = _hit2.collider.gameObject;
                    _temp.SetActive(false);
                    __visibleBy = IsInEyeRange(_playerPos);
                    _temp.SetActive(true);
                    return __visibleBy;
                }
                return false;
            }
            else
            {
                //Debug.Log("test2");
                return true;
            }
        }
        else
        {
            return false;
        }        
    }
}

