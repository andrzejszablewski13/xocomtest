﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HitorMiss : MonoBehaviour
{ 
    private GameObject _textGameObject;

    private TextMeshProUGUI _textComponent;

    // Start is called before the first frame update
    private void Start()
    {

        _textGameObject = gameObject.GetComponentInChildren<TextMeshProUGUI>().gameObject;
        _textComponent = _textGameObject.GetComponent<TextMeshProUGUI>();
    
        _textGameObject.SetActive(false);
    }
    public void OnEnable()
    {
        ShootByPlayer.OnDamageMaking += HitShow;
    }
    public void HitShow(bool _ifhit)
    {
        if(_ifhit==true)
        {
            _textComponent.text = "Hit";
        }else if (_ifhit == false)
        {
            _textComponent.text = "Miss";
        }

        StartCoroutine(ShowText());
    }
    IEnumerator ShowText()
    {
        _textGameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        _textGameObject.SetActive(false);
    }
}
