﻿using System;
using System.Collections;
using UnityEngine;

public class ShootByPlayer : MonoBehaviour
{
    private GameObject _playerObject, _enemyObject;
    private Data _dataAcc;
    private Vector3 _startPos, _endPos;
    private int _roll ;
    public static Action<bool> OnDamageMaking;
    private float _difrenceOfHigh;
    private AudioSource _audioOfShot;
    [SerializeField] private AudioClip clipForShoot;
    // Start is called before the first frame update
   
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        ShootingUI.ToPrepareShoot += PrepShoot;
        _audioOfShot = this.gameObject.GetComponent<AudioSource>();
        _audioOfShot.clip = clipForShoot;
        //PrepShoot();
    }

    // Update is called once per frame
    public void PrepShoot()//prep to shot ( calculate chance to hit)
    {
        if(_dataAcc.EnemyParent.transform.childCount>0)
        {
            _playerObject = _dataAcc.Player[_dataAcc.ActivePlayer];
            _enemyObject = _dataAcc.Enemy[_dataAcc.ActiveEWnemy];
            _startPos.Set(_playerObject.transform.position.x, _playerObject.transform.position.y + 0.75f, _playerObject.transform.position.z);
            _endPos.Set(_enemyObject.transform.position.x+0.5f, _enemyObject.transform.position.y + 0.75f, _enemyObject.transform.position.z+0.5f);
            _dataAcc.DistancePlayerEnemy =_dataAcc.DifBet2Vec2(new Vector2(_startPos.x, _startPos.z), new Vector2(_endPos.x, _endPos.z));           
                _difrenceOfHigh =( _startPos.y - _endPos.y);
            _playerObject.transform.LookAt(_enemyObject.transform.position);
            _dataAcc.ChanceToHit = _dataAcc.HitingProbability(_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].WeaponModyficator, _dataAcc.DistancePlayerEnemy, _difrenceOfHigh);
            _audioOfShot = _dataAcc.Player[_dataAcc.ActivePlayer].GetComponentInChildren<AudioSource>();
        }
    }
    public void ShootNormal()//shoot as name say-active by button
    {
        StartCoroutine(ShootSystem());
    }
    IEnumerator ShootSystem()
    {
        _roll = UnityEngine.Random.Range(1, 100);
        if (_audioOfShot.isPlaying == false) {_audioOfShot.PlayOneShot(clipForShoot); }
        if (_roll <= _dataAcc.ChanceToHit)
        {
            _enemyObject.GetComponent<EnemyBase>().TakeDmg(_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].DamageMaked);
            OnDamageMaking(true);
        }
        else
        {
            OnDamageMaking(false);
        }
        yield return new WaitForSeconds(1);
        _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions = 0;
        _dataAcc.DecisionSetButton = 0;
        _dataAcc.RekuMoveAcc.RecycleSquere();
        _dataAcc.ChangeDecisionSet();       
    }   
}
