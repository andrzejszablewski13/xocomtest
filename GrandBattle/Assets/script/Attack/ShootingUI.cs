﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ShootingUI : MonoBehaviour
{
    // Start is called before the first frame update
    private Data _dataAcc;

    private GameObject[] _textstack;
    private Button _tempButtonRef;
    [SerializeField] private Sprite _enemyIcon;
    private int _numberofbuttons;
    private string _nameAboveEnemy;
    private int _canvasreplacey=159, _canvasrplacex=80;
    private Vector2 _posOnCanvas;
    private List<GameObject> _visEnemy;
    private Vector3 _playerPos;
    private bool _visibilityCheck;
    public static Action ToPrepareShoot;
    
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        Data.OpenShotingUI += OpenUI;
        Data.CloseShotingUI += CloseUI;
    }
private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    void Start()
    {
        _textstack = gameObject.GetAllChilds().ToArray();
        CloseUI();
        _numberofbuttons = 0;
    }
    public void CloseUI()//as name say
    {
        
        _dataAcc.DestroyChildren(_textstack[0].transform);
        
        for (int i = 0; i < _textstack.Length; i++)
        {
            
            _textstack[i].SetActive(false);
        }
        
    }
    private void EndShoting()
    {
        _dataAcc.DecisionSetButton = 0;
        _dataAcc.ChangeDecisionSet();

    }
    public void OpenUI()//as name say
    {
        if(_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions<=0)
        {
            EndShoting();
            return;
        }
        _visEnemy = new List<GameObject>();
        _playerPos = _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Position;
        for (int i = 0; i < _textstack.Length; i++)//set all active
        {
            _textstack[i].SetActive(true) ;
        }
        while(_dataAcc.EniemiesData.GroupOfData[_dataAcc.ActiveEWnemy].IsDeath==true)//if no enemy is alive close
        {
            _dataAcc.ActiveEWnemy++;
            if (_dataAcc.Enemy.Length <= _dataAcc.ActiveEWnemy)
            {
                EndShoting();
                return;
            }
                
            
        }
        
        _posOnCanvas = _dataAcc.AttachUIToGameObject(_dataAcc.Enemy[_dataAcc.ActiveEWnemy].transform.position,1.25f);
        _textstack[2].GetComponent<RectTransform>().localPosition = new Vector3(_posOnCanvas.x + _canvasrplacex, _posOnCanvas.y + _canvasreplacey, 0);
        //set buttons of others targets
        _dataAcc.DestroyChildren(_textstack[0].transform);
        GetStackOfVisEnemy();//create list of vis enemy

        for (int i = 0; i < _visEnemy.Count; i++)//create buttons to check what vis enemies we whant to shot
        {
            CreateEnemySelButton();
        }
        if (_visEnemy.Count == 0)//if no enemy is visible close
        {
            EndShoting();
            return;
        }

        ToPrepareShoot();
    }
    public void GetStackOfVisEnemy()//as name say
    {
        for(int i= 0; i< _dataAcc.Enemy.Length; i++)
        {
            if (_dataAcc.Enemy[i].GetComponent<EnemyBase>()._tempSave.IsDeath == false)
            {
                _playerPos.y += 0.5f;
                
                _visibilityCheck = _dataAcc.Enemy[i].GetComponent<IsVisible>().IsInEyeRange(_playerPos);
                _playerPos.y -= 0.5f;
                if (_visibilityCheck == true)
                {
                    
                    _visEnemy.Add(_dataAcc.Enemy[i]);
                    _dataAcc.ActiveEWnemy = i;
                }
            }
        }
    }
    public void CreateEnemySelButton()//as name say
    {
        _numberofbuttons = _textstack[0].transform.childCount;
        _tempButtonRef = UiTexturedButton(_enemyIcon, new Vector2(25, 25), _textstack[0], _numberofbuttons);
        _tempButtonRef.GetComponent<RectTransform>().localPosition = new Vector3(-75 + 25 * (_numberofbuttons), 26, 0);
        _tempButtonRef.GetComponent<Button>().onClick.AddListener(SetEnemyActive);
    }

    
    public void SetEnemyActive()//as name say
    {
        string _tempPreseedButton = EventSystem.current.currentSelectedGameObject.name;
        
        for (int i = 0; i < _textstack[0].transform.childCount; i++)
        {
            if (_tempPreseedButton.Equals(_textstack[0].transform.GetChild(i).gameObject.name))
            {
                GameObject _temp = _visEnemy[i];
                _dataAcc.ActiveEWnemy = _temp.GetComponent<EnemyBase>().PosOnList;
                _dataAcc.AttackSystem.GetComponent<ShootByPlayer>().PrepShoot();
                _posOnCanvas = _dataAcc.AttachUIToGameObject(_temp.transform.position,1.25f);
               

                _textstack[2].GetComponent<RectTransform>().localPosition = new Vector3(_posOnCanvas.x + 80, _posOnCanvas.y + 159, 0);
              
            }
        }
        SetNameAbove();
    }
    public void SetNameAbove()//as name say- only above active(to shoot) enemy
    {
        _nameAboveEnemy = _dataAcc.EniemiesData.GroupOfData[_dataAcc.ActiveEWnemy].EnemyClass;
        _nameAboveEnemy += " HP:";
        _nameAboveEnemy += _dataAcc.EniemiesData.GroupOfData[_dataAcc.ActiveEWnemy].HP.ToString();
        _nameAboveEnemy += " Chance";
        _nameAboveEnemy += Mathf.RoundToInt(  _dataAcc.ChanceToHit).ToString();
        _nameAboveEnemy += "%";
        _textstack[2].GetComponent<TextMeshProUGUI>().text = _nameAboveEnemy;
    }
    

    public static UnityEngine.UI.Button UiTexturedButton(Sprite sprite, Vector2 size, GameObject canvas, int i)//to creaate button
    {
        GameObject go = new GameObject("Textured button (" + sprite.name + " " + i + ")");

        Image image = go.AddComponent<Image>();
        image.sprite = sprite;

        UnityEngine.UI.Button button = go.AddComponent<UnityEngine.UI.Button>();
        go.transform.SetParent(canvas.transform, false);

        image.rectTransform.sizeDelta = size;

        return button;
    }
}
