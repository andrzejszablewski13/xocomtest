﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MovingUI : MonoBehaviour
{
    private Data _dataAcc;
    private int _enemyN, _playerN, _allN;
    private TextMeshProUGUI[] _textAbove;
    private RectTransform[] _RectForTextAbove;
    private GameObject _temp;
    private float _whaitForNext = 0.1f;
    private string _nameAbove;
    private IEnumerator _corutin;
    private string[] _fragmentsOfStrings;
    // Start is called before the first frame update
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        Data.OpenCloseMovingUI += OnOfMovingUI;
    }
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    public void BeginingOfScript()//as name say
    {
        _fragmentsOfStrings = new string[4] { "TextAbove (", ")", " HP:", " Action:" };
        _corutin = PlacingTextAbove();
        _enemyN = _dataAcc.Enemy.Length;
        _playerN = _dataAcc.Player.Length;
        _allN = _playerN + _enemyN;
      
        _textAbove = new TextMeshProUGUI[_allN];
        _RectForTextAbove = new RectTransform[_allN];
        CreateText(_dataAcc.canvasObject.GetComponentInChildren<MovingUI>().gameObject);
        StartCoroutine(_corutin);
    }
    void CreateText(GameObject canvas)//create gameobjects for each player+enemy
    {
        for (int i = 0; i < _allN; i++)
        {
            _temp=new GameObject(_fragmentsOfStrings[0] + i + _fragmentsOfStrings[1]);
            _textAbove[i] = _temp.AddComponent<TextMeshProUGUI>();
            _textAbove[i].fontSize = 12;
            _textAbove[i].color = Color.red;
            _textAbove[i].alignment = TextAlignmentOptions.Center;
            _temp.AddComponent<ContentSizeFitter>().verticalFit=ContentSizeFitter.FitMode.PreferredSize;

           
            _temp.transform.SetParent(canvas.transform, false);
            _RectForTextAbove[i] = _temp.GetComponent<RectTransform>();
            
        }

    }
    // Update is called once per frame
    IEnumerator PlacingTextAbove()//alocate text on canvas and give text depend to who is attached
    {while(true)
        {
            for (int i = 0; i < _allN; i++)
            {
                
                if (i < _enemyN && (_dataAcc.EniemiesData.GroupOfData[i].IsDeath==false))
                {
                    _textAbove[i].gameObject.SetActive(true);
                    _nameAbove = _dataAcc.EniemiesData.GroupOfData[i].EnemyClass;
                    _nameAbove += _fragmentsOfStrings[2];
                    _nameAbove += _dataAcc.EniemiesData.GroupOfData[i].HP.ToString();
                    _textAbove[i].text = _nameAbove;
                    _RectForTextAbove[i].localPosition = _dataAcc.AttachUIToGameObject(_dataAcc.Enemy[i].transform.position, 1.25f);
                }else if (i < _enemyN && _dataAcc.EniemiesData.GroupOfData[i].IsDeath == true)
                {
                    _textAbove[i].gameObject.SetActive(false);
                }else if(i>= _enemyN &&_dataAcc.PlayerDataStack[i- _enemyN].IsDeath==false)
                {
                    _textAbove[i].gameObject.SetActive(true);
                    _nameAbove = _fragmentsOfStrings[2];
                    _nameAbove += _dataAcc.PlayerDataStack[i - _enemyN].HP.ToString();
                    _nameAbove += _fragmentsOfStrings[3];
                    _nameAbove += _dataAcc.PlayerDataStack[i - _enemyN].Actions.ToString();
                    _textAbove[i].text = _nameAbove;
                    _RectForTextAbove[i].localPosition = _dataAcc.AttachUIToGameObject(_dataAcc.Player[i-_enemyN].transform.position, 1.25f);
                }
                
            }
                        yield return new WaitForSeconds(_whaitForNext);
        }
    }
    IEnumerator EndThis()//as name say
    {
        StopCoroutine(_corutin);
        yield return new WaitForSeconds(_whaitForNext);
        for (int i = 0; i < _textAbove.Length; i++)
        {
            _textAbove[i].gameObject.SetActive(false);
        }
    }
    public void OnOfMovingUI(bool active)//to deactivate/activate
    {
        if (active == false)
        {
            StartCoroutine(EndThis());
        }
        else
        if(active==true)
        {
            StartCoroutine(_corutin);
        }
    }

}
