﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using TMPro;

public class UIBarOne : MonoBehaviour
{
    [SerializeField] private SpriteAtlas SpriteAtlas;
    [SerializeField] private GameObject _spritePreFab;
    private GameObject[] _hpSquere,_actionSquere;
    private GameObject _spaceForHpBar, _spaceForActionBar, _hpText, _actiornText,_objectNameText;
    private Vector3 _position;
    private float _sizeForHp,_sizeForAcction, _maxHp = 10f, _maxAction = 2;
    // Start is called before the first frame update
    private void Start()
    {
        _spaceForHpBar = this.gameObject.transform.GetChild(0).gameObject;
        _spaceForActionBar = this.gameObject.transform.GetChild(1).gameObject;
        MaxHpBar();
        MakeActionBar();
    }
    void MaxHpBar()
    {
        _spritePreFab.gameObject.GetComponent<SpriteRenderer>().sprite = SpriteAtlas.GetSprite("SquareRed");
        _sizeForHp = _spritePreFab.transform.localScale.x;
        _spritePreFab.gameObject.GetComponent<SpriteRenderer>().size = new Vector2(_sizeForHp, 1);
        _position = _spaceForHpBar.transform.position;
        _hpSquere = new GameObject[(int)_maxHp];
        _position = new Vector3(_position.x - _sizeForHp * 0.5f, _position.y, _position.z);
    
        for (int i = 0; i < _maxHp; i++)
        {   
            _hpSquere[i] = Instantiate(_spritePreFab, _position,Quaternion.identity, _spaceForHpBar.transform);
            _position=new Vector3(_position.x - _sizeForHp, _position.y , _position.z);

        }
        _hpText = new GameObject();
        _hpText.transform.parent = _spaceForHpBar.transform;
        if (_maxHp % 2 == 0)
        {
            _hpText.transform.position = new Vector3(_hpSquere[(int)_maxHp / 2].transform.position.x + (_sizeForHp / 2), _hpSquere[(int)_maxHp / 2].transform.position.y, _hpSquere[(int)_maxHp / 2].transform.position.z);
        }
        else
        {
            _hpText.transform.position = new Vector3(_hpSquere[(int)_maxHp / 2].transform.position.x, _hpSquere[(int)_maxHp / 2].transform.position.y, _hpSquere[(int)_maxHp / 2].transform.position.z);
        }
        _hpText.AddComponent<TextMeshPro>().text = "Hp:" + _maxHp.ToString();
                AddTextToBar(_hpText);

        _position = new Vector3(_hpText.transform.position.x, _hpText.transform.position.y + _hpText.transform.localScale.y, _hpText.transform.position.z);
        AddName(_position);
    }
    void MakeActionBar()
    {
        _spritePreFab.gameObject.GetComponent<SpriteRenderer>().sprite=SpriteAtlas.GetSprite("SquareBlue");
        _sizeForAcction= _spritePreFab.transform.localScale.x*((_maxHp/2)/_maxAction);
        _spritePreFab.gameObject.GetComponent<SpriteRenderer>().size = new Vector2(_sizeForAcction, 1);
        _position = _spaceForActionBar.transform.position;
        _actionSquere = new GameObject[(int)_maxAction];
        _position = new Vector3(_position.x - _sizeForAcction * 0.5f, _position.y, _position.z);
        for (int i = 0; i < _maxAction; i++)
        {
            
            _actionSquere[i] = Instantiate(_spritePreFab, _position, Quaternion.identity, _spaceForActionBar.transform);
            _position = new Vector3(_position.x - _sizeForAcction, _position.y, _position.z);

        }

        _actiornText = new GameObject();
        _actiornText.transform.parent = _spaceForActionBar.transform;
        if (_maxAction % 2 == 0)
        {
            _actiornText.transform.position = new Vector3(_actionSquere[(int)_maxAction / 2].transform.position.x + (_sizeForHp / 2), _actionSquere[(int)_maxAction / 2].transform.position.y, _actionSquere[(int)_maxAction / 2].transform.position.z);
        }
        else
        {
            _actiornText.transform.position = new Vector3(_actionSquere[(int)_maxAction / 2].transform.position.x, _actionSquere[(int)_maxAction / 2].transform.position.y, _actionSquere[(int)_maxAction / 2].transform.position.z);
        }
        _actiornText.AddComponent<TextMeshPro>().text = "Action:" + _maxAction.ToString();
        AddTextToBar(_actiornText);

    }
    // Update is called once per frame
    private void AddName(Vector3 _positionlocal)
    {
        _objectNameText = new GameObject();
        _objectNameText.transform.parent = _spaceForHpBar.transform;
        _objectNameText.transform.position = _positionlocal;
        _objectNameText.AddComponent<TextMeshPro>().text = "NameOfObject";
        AddTextToBar(_objectNameText);

    }
    private void AddTextToBar(GameObject _placeForText)
    {
        _placeForText.GetComponent<TextMeshPro>().fontSize = 12;
        _placeForText.GetComponent<TextMeshPro>().autoSizeTextContainer = true;
        _placeForText.GetComponent<TextMeshPro>().sortingOrder = 1;
        _placeForText.transform.localRotation = new Quaternion(0, 180, 0, 0);
    }
}