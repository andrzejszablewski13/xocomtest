﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RekuMove : MonoBehaviour
{
    private GameObject _squareRecycler, _stackOfVisibleSquere, _temporalGameObject;
    private Data _dataAcc;
    private int _lineNumber, _minNStep, _tempkey;
    private Vector3 _player, _playerstart, _playerEnd, _tempfloat;
    private Vector3Int _playerV2, _playerEndInt;

    private int _grafTemp11, _grafTemp2, _grafTemp3;
    private List<int> _stackOfKeys;
    private Dictionary<int, int> __stackOfUniqueKeys;
    private Data.SquareInGame _temporal, _tempAb;
    private LineRenderer _lineReference;
    private int _startActiveKey,_activePlalerNumber;
    public static Action<int> PlayerMoveStartAnim;



    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
        
    }


    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        _lineReference = gameObject.transform.GetChild(0).gameObject.GetComponent<LineRenderer>();
        _stackOfVisibleSquere = gameObject.transform.GetChild(1).gameObject;
        _squareRecycler = gameObject.transform.GetChild(2).gameObject;
        _squareRecycler.SetActive(false);
        __stackOfUniqueKeys = new Dictionary<int, int>();
        AnimationSystem.PlayerMoveEndAnim += EndPlayerMovement;
    }


   public void MovePlayerTo()//as name say
    {
        if (_dataAcc.MoousePosition != Vector3.down)
        {
            _activePlalerNumber = _dataAcc.ActivePlayer;
            _dataAcc.PlayerDataStack[_activePlalerNumber].Position = _dataAcc.MoousePosition;
            _dataAcc.PlayerDataStack[_activePlalerNumber].Position.y = Mathf.RoundToInt(_dataAcc.PlayerDataStack[_activePlalerNumber].Position.y);
            _dataAcc.PlayerDataStack[_activePlalerNumber].Position.x *= _dataAcc.SizeX;
            _dataAcc.PlayerDataStack[_activePlalerNumber].Position.y *= _dataAcc.SizeY;
            _dataAcc.PlayerDataStack[_activePlalerNumber].Position.z *= _dataAcc.SizeZ;//change player loc in script base
            _dataAcc.MoousePosition = Vector3.down;

            _grafTemp11 = Mathf.RoundToInt(_dataAcc.Player[_activePlalerNumber].transform.position.x);
            _grafTemp2 = Mathf.RoundToInt(_dataAcc.Player[_activePlalerNumber].transform.position.z);
            _grafTemp3 = Mathf.RoundToInt(_dataAcc.Player[_activePlalerNumber].transform.position.y);
            _tempkey = _dataAcc.Ultramuch + _grafTemp11 * _dataAcc.VeryMuch + _grafTemp3 * _dataAcc.Much + _grafTemp2;//creaate key to dic

            _temporal = _dataAcc.MapStack[_tempkey];
            _temporal.TypeOfObject= Data.BlockType.Empty;
            _dataAcc.MapStack[_tempkey] = _temporal;//change squere in dic  to empty (no player here now)
            PlayerMoveStartAnim(_activePlalerNumber);
            RecycleSquere();//clear movement traffic

        }
    
    }
    private void EndPlayerMovement(int _activePlayerID)//as name say
    {
        
        _grafTemp11 = Mathf.RoundToInt(_dataAcc.Player[_activePlayerID].transform.position.x) / _dataAcc.SizeX;
        _grafTemp2 = Mathf.RoundToInt(_dataAcc.Player[_activePlayerID].transform.position.z) / _dataAcc.SizeZ;
        _grafTemp3 = Mathf.RoundToInt(_dataAcc.Player[_activePlayerID].transform.position.y) / _dataAcc.SizeY;
        _tempkey = _dataAcc.Ultramuch + _grafTemp11 * _dataAcc.VeryMuch + _grafTemp3 * _dataAcc.Much + _grafTemp2;//creaate key to dic
        _temporal = _dataAcc.MapStack[_tempkey];
        _temporal.TypeOfObject = Data.BlockType.Player;
        _dataAcc.MapStack[_tempkey] = _temporal;//change squere in dic  to player ( player is here now)

        _dataAcc.Player[_activePlayerID].SetActive(true);


        if (_minNStep > _dataAcc.PlayerDataStack[_activePlayerID].MaxMoves / 2)
        {
            _dataAcc.PlayerDataStack[_activePlayerID].Actions--;
            _dataAcc.PlayerDataStack[_activePlayerID].LeftMoves -= _dataAcc.PlayerDataStack[_activePlayerID].MaxMoves / 2;
        }
        else
        {
            _dataAcc.PlayerDataStack[_activePlayerID].Actions -= 2;
            if(_dataAcc.PlayerDataStack[_activePlayerID].Actions<0)
            {
                _dataAcc.PlayerDataStack[_activePlayerID].Actions = 0;
            }
        }//change number of action
        if (_activePlayerID == _dataAcc.ActivePlayer)
        {
            _dataAcc.Rekustarttest = true;
            MovementOnDictionary();//create new movement traffic
        }
    }
    public void DrawMoveLineFromDic()//as name say
    {
        _lineNumber = 0;
        if (_dataAcc.MousePositionWClick != Vector3.down && _stackOfVisibleSquere.transform.childCount > 0)//work when mouse in movement distance
        {
           
            _playerstart = _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Position;
            
            _playerstart.y = Mathf.RoundToInt(_playerstart.y);//get start point
            _playerEnd = _dataAcc.MousePositionWClick;
            _playerEndInt.y = Mathf.RoundToInt(_playerEnd.y);
            _playerEndInt.z = Mathf.RoundToInt(_playerEnd.z);
            _playerEndInt.x = Mathf.RoundToInt(_playerEnd.x);//get end point
            _tempkey = _dataAcc.Ultramuch + _playerEndInt.x / _dataAcc.SizeX * _dataAcc.VeryMuch + _playerEndInt.y / _dataAcc.SizeY * _dataAcc.Much + _playerEndInt.z / _dataAcc.SizeZ;
            if (_dataAcc.MapStack.ContainsKey(_tempkey))
            {
                _minNStep = _dataAcc.MapStack[_tempkey].Steps;//get number of needed steps
                _dataAcc.MovementStackCollection = new Stack<Vector3>();//for animation
                _tempfloat = _playerEndInt; _tempfloat.x *= _dataAcc.SizeX; _tempfloat.y *= _dataAcc.SizeY; _tempfloat.z *= _dataAcc.SizeZ;
                for (int i = _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].LeftMoves; i >= _minNStep; i--)//create shortest way from start to end point and create line
                {
                    if (_tempkey > 100)
                    {
                        AddLine();
                        _tempfloat.x = (_tempkey % (_dataAcc.Ultramuch/9)) / _dataAcc.VeryMuch;
                        _tempfloat.y = (_tempkey % _dataAcc.VeryMuch) / _dataAcc.Much;
                        _tempfloat.z = _tempkey % _dataAcc.Much;
                        _tempfloat.x *= _dataAcc.SizeX; _tempfloat.y *= _dataAcc.SizeY; _tempfloat.z *= _dataAcc.SizeZ;
                        _dataAcc.MovementStackCollection.Push(_tempfloat);//for animation
                        _tempfloat.y += 0.25f;
                        _lineReference.SetPosition(_lineNumber - 1, _tempfloat);
                        _tempkey = _dataAcc.MapStack[_tempkey].Previus;
                    }
                }

            }
        }
        else { _lineReference.positionCount = 0; }
    }
    void AddLine()//as name say
    {
        _lineNumber++;
        _lineReference.positionCount = _lineNumber;
    }

    
    public void RecycleSquere()
    {
        for (int i = 0; i < _stackOfKeys.Count; i++)
        {
            _tempAb = _dataAcc.MapStack[_stackOfKeys[i]];
            _tempAb.Steps = 0;
            _tempAb.Previus = 0;
            _tempAb.MoveSqarePossed = false;
            _dataAcc.MapStack[_tempAb.Key] = _tempAb;
            _stackOfVisibleSquere.transform.GetChild(0).gameObject.transform.SetParent(_squareRecycler.transform);
        }
        
       
        _stackOfKeys.Clear();
    }
    

    public void MovementOnDictionary()//as name say- to create and start recursion
    {

        if (_dataAcc.Rekustarttest == true 
           
            && _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions > 0)
        {
            _dataAcc.Rekustarttest = false;
            _stackOfKeys = new List<int>();
            _player = _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Position;
            _playerV2 = Vector3Int.RoundToInt(_player);
            _playerV2.x /= _dataAcc.SizeX;
            _playerV2.y /= _dataAcc.SizeY;
            _playerV2.z /= _dataAcc.SizeZ;
            _startActiveKey = _dataAcc.Ultramuch + _playerV2.x * _dataAcc.VeryMuch + _playerV2.y * _dataAcc.Much + _playerV2.z;
            _stackOfKeys.Add(_startActiveKey);
            __stackOfUniqueKeys.Clear();//dictionary to check if keys are unique
            __stackOfUniqueKeys.Add(_startActiveKey, 0);
            RekuOnDictionary(0, _startActiveKey, (byte)_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].LeftMoves);
        }
    }
    
    void RekuOnDictionary(int _previuskey,int _activekey,byte steps)//RECURIOSN!!!
    {
        
        Data.SquareInGame _temp = _dataAcc.MapStack[_activekey];
       
        if (_temp.Steps < steps)
        {
            if(_temp.MoveSqarePossed==false)
            {
                _temporalGameObject = _squareRecycler.transform.GetChild(0).gameObject;
                _temporalGameObject.transform.SetParent(_stackOfVisibleSquere.transform);
                _temporalGameObject.transform.position = new Vector3((_activekey - _dataAcc.Ultramuch) / _dataAcc.VeryMuch, (_activekey % _dataAcc.VeryMuch) / _dataAcc.Much + 0.1f, _activekey % _dataAcc.Much);
            }

            _temp.Steps = steps;
            _temp.Previus = _previuskey;
            _temp.MoveSqarePossed = true;
            _dataAcc.MapStack[_activekey] = _temp;
            if (!__stackOfUniqueKeys.ContainsKey(_activekey))
            {
                __stackOfUniqueKeys.Add(_activekey, 0);
          _stackOfKeys.Add(_activekey);
            }
            steps--;
            if (steps > 0)
            {
                for (int i = 0; i < _temp.NeighborsProbablyFree.Count; i++)
                {
                    
                    if (_temp.NeighborsProbablyFree[i] != _previuskey && _dataAcc.MapStack[_temp.NeighborsProbablyFree[i]].Steps < steps
                        && _dataAcc.MapStack[_temp.NeighborsProbablyFree[i]].TypeOfObject==Data.BlockType.Empty)
                    {
                      RekuOnDictionary(_activekey, _temp.NeighborsProbablyFree[i], steps);
                    }
                }
            }
        }
    }
}



