﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour//wylacznie ruch kamery
{

    private Rigidbody rd;
    private float getx, gety,getz;
    private Vector3 movement;
    public float _speed = 25,slowing=0.8f,_speed2=200;
    public IEnumerator Corutin;
    void Start()
    {
        rd = gameObject.GetComponent<Rigidbody>();
        Corutin = CamreMovement();
        StartCoroutine(Corutin);
    }

    
    IEnumerator CamreMovement()
    {
        while (true)
        {
            getx = Input.GetAxis("Horizontal");
            gety = Input.GetAxis("Vertical");
            getz = Input.GetAxis("Mouse ScrollWheel");


            movement = new Vector3(getx, gety, getz * _speed2);
            rd.AddRelativeForce(movement * _speed* Time.deltaTime, ForceMode.VelocityChange);
            if (getx == 0 && gety == 0 && rd.velocity != Vector3.zero) rd.velocity *= slowing;
            yield return new WaitForEndOfFrame();
        }
    }
}
