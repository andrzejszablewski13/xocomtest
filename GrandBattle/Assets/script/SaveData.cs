﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SaveDataFile", menuName = "SaveMap")]
public class SaveData : ScriptableObject
{
    // Start is called before the first frame update
    
    public Vector3[] _PlayerVectors;
    public Data.BlockType[,,] _stack2;
    public int _PlayerNumbers, _mapSize1, _mapSize2, _mapSize3;
    public bool Saved;
    public int _activePlayer;
}
