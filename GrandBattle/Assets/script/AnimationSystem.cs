﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSystem : MonoBehaviour
{
   

    private Data _dataAcc;
    public enum Direction : byte
    {
    Stop,North,South,West,East,Up,Down
    }//south-east has cord 0-0
   /* private Direction _directOfMovement;
    private GameObject _objectToAnim;
    private Animator _animControler;
    private Rigidbody _rb;
    private Vector3 _velocity;
    private Vector3[] _nextSteps;
    private int _numbersOfSteps,_actualStep;
    private Vector3 _rotationOfMovement;
    private bool _incJump,_flying;*/
    public struct DataForAnimContr
    {
        public Direction _directOfMovement;
        public GameObject _objectToAnim;
        public Animator _animControler;
        public Rigidbody _rb;
        public Vector3 _velocity;
        public Vector3[] _nextSteps;
        public int _numbersOfSteps, _actualStep;
        public Vector3 _rotationOfMovement;
        public bool _incJump, _flying;
    }
    private DataForAnimContr _tempStruct;
    private string[] _stackOfTriggers;
    public float _speed = 250, _speed2 = 250, _falsegravity = 1000, _jumpHigher = 250, _jumpLower = 250, _speedToGoUp = 25, _whaitForNext = 0.05f;
    public static Action<int> PlayerMoveEndAnim;
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        RekuMove.PlayerMoveStartAnim += MovingAnimation;
        _stackOfTriggers = new string[3] { "RUN", "END", "JUMP" };
    }
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    public void MovingAnimation(int _activePlayer)//begin prerp to start anim
    {
        _tempStruct = new DataForAnimContr
        {
            _directOfMovement = Direction.Stop,
            _objectToAnim = _dataAcc.Player[_activePlayer],
            _nextSteps = _dataAcc.MovementStackCollection.ToArray(),
            _actualStep = 0,
            _incJump = false,
            _flying = false,
            _velocity = Vector3.zero,
            _rotationOfMovement = Vector3.zero
        };
        _tempStruct._rb = _tempStruct._objectToAnim.GetComponent<Rigidbody>();
        _tempStruct._animControler = _tempStruct._objectToAnim.GetComponent<Animator>();
        _tempStruct._numbersOfSteps = _tempStruct._nextSteps.Length;
        //_tempStruct._nextSteps = _dataAcc.MovementStackCollection.ToArray();
        /*_tempStruct._actualStep = 0;
        _tempStruct._incJump = false;
        _tempStruct._flying = false;*/
        /*_tempStruct._velocity = Vector3.zero;
        _tempStruct._rotationOfMovement = Vector3.zero;*/

        StartCoroutine(Running(_activePlayer, _tempStruct));

       
    }
    
   public IEnumerator Running(int _activePlayer, DataForAnimContr _structWitchData)//control movement of animation
    {
        while (true)
        {
            switch (_structWitchData._directOfMovement)
            {
                case Direction.Stop://decide where to go
                    _structWitchData._actualStep++;
                    if (_structWitchData._actualStep < _structWitchData._numbersOfSteps)
                    {
                        if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].y > _structWitchData._nextSteps[_structWitchData._actualStep].y)
                        {
                            _structWitchData._directOfMovement = Direction.Down;
                            _structWitchData._incJump = false;
                            _structWitchData._animControler.SetBool(_stackOfTriggers[0], false);

                            _structWitchData._animControler.SetTrigger(_stackOfTriggers[1]);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].y < _structWitchData._nextSteps[_structWitchData._actualStep].y)
                        {
                            _structWitchData._directOfMovement = Direction.Up;
                            _structWitchData._incJump = false;
                            _structWitchData._animControler.SetBool(_stackOfTriggers[0], false);
                            _structWitchData._flying = false;
                            _structWitchData._animControler.SetTrigger(_stackOfTriggers[1]);
                        }
                        else
                        {
                            if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x > _structWitchData._nextSteps[_structWitchData._actualStep].x)
                            {
                                _structWitchData._directOfMovement = Direction.South;
                                _structWitchData._velocity = Vector3.left;
                                _structWitchData._rotationOfMovement = new Vector3(0, 270, 0);
                                _structWitchData._animControler.SetBool(_stackOfTriggers[0], true);
                            }
                            else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x < _structWitchData._nextSteps[_structWitchData._actualStep].x)
                            {
                                _structWitchData._directOfMovement = Direction.North;
                                _structWitchData._velocity = Vector3.right;
                                _structWitchData._rotationOfMovement = new Vector3(0, 90, 0);
                                _structWitchData._animControler.SetBool(_stackOfTriggers[0], true);
                            }
                            if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z > _structWitchData._nextSteps[_structWitchData._actualStep].z)
                            {
                                _structWitchData._directOfMovement = Direction.East;
                                _structWitchData._velocity = Vector3.back;
                                _structWitchData._rotationOfMovement = new Vector3(0, 180, 0);
                                _structWitchData._animControler.SetBool(_stackOfTriggers[0], true);
                            }
                            else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z < _structWitchData._nextSteps[_structWitchData._actualStep].z)
                            {
                                _structWitchData._directOfMovement = Direction.West;
                                _structWitchData._velocity = Vector3.forward;
                                _structWitchData._rotationOfMovement = new Vector3(0, 0, 0);
                                _structWitchData._animControler.SetBool(_stackOfTriggers[0], true);
                            }
                        }
                    }
                    else {
                       
                    }
                    break;

                case Direction.Up://if jump higher
                    if (_structWitchData._incJump == true)
                    {
                        _structWitchData._rb.AddForce((_structWitchData._velocity * _speedToGoUp * Time.deltaTime), ForceMode.Impulse);
                        if (_structWitchData._flying == false) 
                        {
                            _structWitchData._rb.AddForce((Vector3.down * _falsegravity * Time.deltaTime), ForceMode.Impulse);
                        }
                        if(_structWitchData._objectToAnim.transform.position.y >= _structWitchData._nextSteps[_structWitchData._actualStep].y)
                        {
                            _structWitchData._flying = false;
                        }else
                        if (_structWitchData._objectToAnim.transform.position.y <= _structWitchData._nextSteps[_structWitchData._actualStep].y && _structWitchData._flying ==false)
                        {
                            _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                            _structWitchData._directOfMovement = Direction.Stop;
                        }
                    }
                    else
                    {

                        _structWitchData._rb.AddForce((Vector3.up*_jumpHigher), ForceMode.Impulse);
                        _structWitchData._animControler.SetTrigger(_stackOfTriggers[2]);
                        if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x > _structWitchData._nextSteps[_structWitchData._actualStep].x)
                        {

                            _structWitchData._velocity = Vector3.left;
                            _structWitchData._rotationOfMovement = new Vector3(0, 270, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x < _structWitchData._nextSteps[_structWitchData._actualStep].x)
                        {

                            _structWitchData._velocity = Vector3.right;
                            _structWitchData._rotationOfMovement = new Vector3(0, 90, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z > _structWitchData._nextSteps[_structWitchData._actualStep].z)
                        {

                            _structWitchData._velocity = Vector3.back;
                            _structWitchData._rotationOfMovement = new Vector3(0, 180, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z < _structWitchData._nextSteps[_structWitchData._actualStep].z)
                        {

                            _structWitchData._velocity = Vector3.forward;
                            _structWitchData._rotationOfMovement = new Vector3(0, 0, 0);

                        }
                        _structWitchData._objectToAnim.transform.eulerAngles = _structWitchData._rotationOfMovement;
                        _structWitchData._incJump = true;
                        _structWitchData._flying = true;

                    }
                    break;
                case Direction.Down://if jump lower
                    if(_structWitchData._incJump ==true)
                    {
                        _structWitchData._rb.AddForce((_structWitchData._velocity * _speed2 * Time.deltaTime), ForceMode.Impulse);
                        _structWitchData._rb.AddForce((Vector3.down*_falsegravity*Time.deltaTime), ForceMode.Impulse);
                        if(_structWitchData._objectToAnim.transform.position.y <= _structWitchData._nextSteps[_structWitchData._actualStep].y)
                        {
                            _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                            _structWitchData._directOfMovement = Direction.Stop;
                        }
                    }
                    else
                    {

                        _structWitchData._rb.AddForce((Vector3.up), ForceMode.Impulse);
                        _structWitchData._animControler.SetTrigger(_stackOfTriggers[2]);
                        if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x > _structWitchData._nextSteps[_structWitchData._actualStep].x)
                        {

                            _structWitchData._velocity = Vector3.left;
                            _structWitchData._rotationOfMovement = new Vector3(0, 270, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].x < _structWitchData._nextSteps[_structWitchData._actualStep].x)
                        {

                            _structWitchData._velocity = Vector3.right;
                            _structWitchData._rotationOfMovement = new Vector3(0, 90, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z > _structWitchData._nextSteps[_structWitchData._actualStep].z)
                        {

                            _structWitchData._velocity = Vector3.back;
                            _structWitchData._rotationOfMovement = new Vector3(0, 180, 0);
                        }
                        else if (_structWitchData._nextSteps[_structWitchData._actualStep - 1].z < _structWitchData._nextSteps[_structWitchData._actualStep].z)
                        {

                            _structWitchData._velocity = Vector3.forward;
                            _structWitchData._rotationOfMovement = new Vector3(0, 0, 0);

                        }
                        _structWitchData._objectToAnim.transform.eulerAngles = _structWitchData._rotationOfMovement;
                        _structWitchData._incJump = true;

                    }

                    break;

                default://move horizontaly
                        // _objectToAnim.transform.rotation = _rotationOfMovement;
                    _structWitchData._objectToAnim.transform.eulerAngles= _structWitchData._rotationOfMovement;
                    _structWitchData._rb.AddForce((_structWitchData._velocity * _speed *Time.deltaTime), ForceMode.Impulse);
                    if (_structWitchData._actualStep < _structWitchData._numbersOfSteps) { 
                        switch (_structWitchData._directOfMovement)
                    {

                        case Direction.North:
                            if (_structWitchData._objectToAnim.transform.position.x > _structWitchData._nextSteps[_structWitchData._actualStep].x)
                            {
                                    _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                                    _structWitchData._directOfMovement = Direction.Stop;
                            }
                            break;
                        case Direction.South:
                            if (_structWitchData._objectToAnim.transform.position.x < _structWitchData._nextSteps[_structWitchData._actualStep].x)
                            {
                                    _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                                    _structWitchData._directOfMovement = Direction.Stop;
                            }
                            break;
                        case Direction.West:
                            if (_structWitchData._objectToAnim.transform.position.z > _structWitchData._nextSteps[_structWitchData._actualStep].z)
                            {
                                    _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                                    _structWitchData._directOfMovement = Direction.Stop;
                            }
                            break;
                        case Direction.East:
                            if (_structWitchData._objectToAnim.transform.position.z < _structWitchData._nextSteps[_structWitchData._actualStep].z)
                            {
                                    _structWitchData._objectToAnim.transform.position = _structWitchData._nextSteps[_structWitchData._actualStep];
                                    _structWitchData._directOfMovement = Direction.Stop;
                            }
                            break;
                    }
                    }

                    break;
            }
            if (_structWitchData._actualStep <= _structWitchData._numbersOfSteps)//continue control of animation
            {
               
                yield return new WaitForSeconds(_whaitForNext); 
            }
            else
            { 
                
                break;
            }

        }
        _structWitchData._rb.velocity = Vector3.zero;
        _dataAcc.GameTurn = Data.Turn.PlayerTurn;
        _structWitchData._animControler.SetBool(_stackOfTriggers[0], false);
        _structWitchData._animControler.SetTrigger(_stackOfTriggers[1]);
        EndAnim(_activePlayer);
        
    }
    private void EndAnim(int _activePlayer)
    {
        PlayerMoveEndAnim(_activePlayer);
    }
}
