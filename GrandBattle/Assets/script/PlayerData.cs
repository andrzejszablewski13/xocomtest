﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SaveCharStat", menuName = "CharacterStat")]
public class PlayerData : ScriptableObject
{
    [SerializeField]private int _MaxMoves=10,_MaxHP=10;
    
    public int HP,Actions/*number of action player can take*/,LeftMoves;
    public Vector3 Position;
    
    public int MaxMoves { get => _MaxMoves;}
    public int MaxHP { get => _MaxHP; }

    public int DamageMaked = 5, _weaponModyficator = 90;
    public bool IsDeath;
    public string ClassOfCharacter, NameOfPreFab = "Player1 Variant";
    public int WeaponModyficator { get => _weaponModyficator; }
    
}
