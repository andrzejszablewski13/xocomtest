﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Ray : MonoBehaviour
{
    private Data _dataAcc;
    private UnityEngine.Ray _ray;
    private RaycastHit _hit;
    public IEnumerator Corutin;
    private readonly int layerMask = 1 << 8;//work only on layer 8-muse_hit
    private int _tempKey;
    public static Action UseOrFire;
    void Awake()
    {
        Data.AccessToData += GetAccessToData;
        Data.StrategicMapStart += BeginCamera;
    }
   
    private void GetAccessToData(Data _dataAccess)
    {
        _dataAcc = _dataAccess;
    }
    // Update is called once per frame
   private void BeginCamera()
    {
        Corutin = InputOnMuvement();
        StartCoroutine(Corutin);
    }
    IEnumerator InputOnMuvement()//asnamesay
    {
        while (true)
        {
            if (_dataAcc.DecisionSet == Data.DecisionType.Move)
            {
                NewClickSystem();
            }else if(_dataAcc.DecisionSet == Data.DecisionType.Spell)
            {
                SpellsControl();
            }
            yield return new WaitForFixedUpdate();
        }
    }
    void NewClickSystem()//asnamesay
    {
       
            _ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);//_raycast mouse 
            _dataAcc.MousePositionWClick = Vector3.down;
            _dataAcc.MoousePosition = Vector3.down;
            if (!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(_ray, out _hit, 100, layerMask))
            {
           
               if(!_hit.collider.gameObject.CompareTag(Data.Tags.Player.ToString()))
                {
                    _dataAcc.MousePositionWClick = new Vector3(Mathf.RoundToInt(_hit.point.x / _dataAcc.SizeX), _hit.point.y / _dataAcc.SizeY, Mathf.RoundToInt(_hit.point.z / _dataAcc.SizeZ));//to draw a line
                    _dataAcc.RekuMoveAcc.DrawMoveLineFromDic();
                  
                    
                }
              
                if (Input.GetMouseButtonDown(0))
                {
                    
                    //print("Found an object "+_hit.collider.gameObject.name + " distance: " + _hit.distance);
                    if (_hit.collider.CompareTag(Data.Tags.Player.ToString()))
                    {
                        _dataAcc.RayHitName = _hit.collider.gameObject.name; _dataAcc.RayHitNameTest = true;
                        _dataAcc.ChangePlayerCharacter();
                    } //if _hit player
                    else
                    {//_hit space
                        _dataAcc.MoousePosition = new Vector3(Mathf.RoundToInt(_hit.point.x / _dataAcc.SizeX), _hit.point.y / _dataAcc.SizeY, Mathf.RoundToInt(_hit.point.z / _dataAcc.SizeZ));

                        _tempKey = _dataAcc.Ultramuch + Mathf.RoundToInt(_dataAcc.MoousePosition.x / _dataAcc.SizeX) * _dataAcc.VeryMuch + Mathf.RoundToInt(_dataAcc.MoousePosition.y / _dataAcc.SizeY) * _dataAcc.Much + Mathf.RoundToInt(_dataAcc.MoousePosition.z / _dataAcc.SizeZ);
                        try
                        {
                            if (_dataAcc.MapStack[_tempKey].TypeOfObject == Data.BlockType.Empty && _dataAcc.MapStack[_tempKey].Steps > 0
                                               )//+_hit free position
                            {

                                _dataAcc.RekuMoveAcc.MovePlayerTo();
                            }
                            else
                            {
                                _dataAcc.MoousePosition = Vector3.down;
                            }
                        }
                        catch (System.Exception)
                        {
                            Debug.Log(_tempKey);
                        }
                        
                    }
                }
                
            }
            else
            {
                _dataAcc.MousePositionWClick = Vector3.down;
                _dataAcc.RekuMoveAcc.DrawMoveLineFromDic();
            }
        
    }

    void SpellsControl()
    {
       
       
            
            _ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);//_raycast mouse 
            _dataAcc.MousePositionWClick = Vector3.down;
            _dataAcc.MoousePosition = Vector3.down;
            if (!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(_ray, out _hit, 100, layerMask))
            {
            float _distance = Vector2.Distance(new Vector2(_hit.point.x, _hit.point.z), new Vector2(_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Position.x, _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Position.z));
                if (_distance <= 15/2)
                {
                    _dataAcc.SpellsRangeObject.transform.position = (new Vector3(Mathf.RoundToInt(_hit.point.x / _dataAcc.SizeX), 0, Mathf.RoundToInt(_hit.point.z / _dataAcc.SizeZ)));

                    if (Input.GetMouseButtonDown(0))
                    {

                        {
                            UseOrFire();
                            _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions -= 2;
                            if (_dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions <= 0)
                            {
                                _dataAcc.PlayerDataStack[_dataAcc.ActivePlayer].Actions = 0;
                                _dataAcc.DecisionSetButton = 0;
                                _dataAcc.ChangeDecisionSet();
                            }
                        }
                    }
                }
            }
        
    }
}
